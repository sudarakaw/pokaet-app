/* ui/js/init.js: entry point Js module for pokaet app
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

(() => {
  'use strict'

  const app = Elm.Main.init({ 'node': document.getElementById('app') })

  // Select content of input box on ports signal sent from Elm
  app.ports.selectInput.subscribe(id => {
    document.getElementById(`input_${id}`).select()
  })

  document
    .querySelectorAll('.external-link')
    .forEach(elm => {
      elm.addEventListener('click', e => {
        e.preventDefault()

        __TAURI__.core.invoke('open_external_link', { 'url': e.target.href })
      })
    })
})()
