// build.rs: custom build script
//
// Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY;
// This is free software, and you are welcome to redistribute it and/or modify it
// under the terms of the BSD 2-clause License. See the LICENSE file for more
// details.
//

use std::{fs, process::Command};

#[cfg(not(debug_assertions))]
use grass::OutputStyle;

use minijinja::render;

fn main() {
    // Generate ui/index.html
    {
        println!("cargo::rerun-if-changed=src/index.html");

        fs::write(
            "ui/index.html",
            render!(include_str!("src/index.html"), VERSION => env!("CARGO_PKG_VERSION")),
        )
        .unwrap();
    }

    // Generate ui/css/styles.css
    {
        println!("cargo::rerun-if-changed=src/styles.sass");

        let options = grass::Options::default();

        #[cfg(not(debug_assertions))]
        let options = options.style(OutputStyle::Compressed);

        fs::write(
            "ui/css/styles.css",
            grass::from_path("src/styles.sass", &options).unwrap(),
        )
        .unwrap();
    }

    // Build Elm app
    {
        println!("cargo:rerun-if-changed=src/Main.elm");

        let mut command = Command::new("elm");

        command.arg("make");

        #[cfg(debug_assertions)]
        command.arg("--debug");

        #[cfg(not(debug_assertions))]
        command.arg("--optimize");

        command.arg("--output=ui/js/app.js").arg("src/Main.elm");

        let output = command.output().expect("Failed to run `elm make`,");

        if !output.status.success() {
            println!("{}", String::from_utf8_lossy(&output.stderr));

            std::process::exit(1);
        }
    }

    tauri_build::build()
}
