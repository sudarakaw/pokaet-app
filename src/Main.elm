-- src/Main.elm: main module for the Elm application
--
-- Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--


port module Main exposing (main)

import Browser
import FormatNumber
import FormatNumber.Locales exposing (Decimals(..), Locale, System(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onFocus, onInput)



-- VIEW


view : Model -> Html Msg
view model =
    let
        totalText =
            model.denominations
                |> List.map (Tuple.mapFirst denominationAmount)
                |> List.map (\( amount, count ) -> amount * count)
                |> List.sum
                |> formatRupee

        body_ =
            node "main"
                [ class "container" ]
                ((model.denominations |> List.map renderDenomination)
                    ++ [ div
                            [ class
                                ("result rounded-3 text-white"
                                    ++ (if "" == totalText then
                                            " empty"

                                        else
                                            ""
                                       )
                                )
                            ]
                            [ h1 [ title totalText ]
                                [ if "" == totalText then
                                    text "Enter number of notes you have for each denomination."

                                  else
                                    text totalText
                                ]
                            , if "" == totalText then
                                text ""

                              else
                                button
                                    [ type_ "button"
                                    , class "btn btn-light"
                                    , onClick ClearAll
                                    , tabindex -1
                                    ]
                                    [ text "×" ]
                            ]
                       ]
                )
    in
    body_


renderDenomination : DenominationCount -> Html Msg
renderDenomination ( denomination, count ) =
    let
        amount =
            denomination |> denominationAmount

        input_id =
            "input_" ++ (amount |> String.fromInt)
    in
    div [ class "input-group input-group-lg mb-1" ]
        [ label [ class "input-group-text", for input_id ]
            [ text (amount |> formatRupee) ]
        , input
            [ type_ "number"
            , id input_id
            , class "form-control"
            , attribute "min" "0"
            , step "1"
            , value (count |> String.fromInt)
            , onInput (CountChange denomination)
            , onFocus (SelectContent amount)
            , autofocus (5000 == amount)
            ]
            []
        , div [ class "input-group-text bg" ]
            [ text ((amount * count) |> formatRupee) ]
        , button
            [ type_ "button"
            , class "btn btn-outline-light"
            , onClick (CountChange denomination "0")
            , disabled (0 == count)
            , tabindex -1
            ]
            [ text "×" ]
        ]


formatRupee : Int -> String
formatRupee value =
    let
        locale =
            Locale (Max 0) Western "," "" "රු" "" "රු" "" "" ""
    in
    if 1 > value then
        ""

    else
        value |> toFloat |> FormatNumber.format locale


denominationAmount : Denomination -> Int
denominationAmount denomination =
    case denomination of
        FiveThousand ->
            5000

        OneThousand ->
            1000

        FiveHundred ->
            500

        OneHundred ->
            100

        Fifty ->
            50

        Twenty ->
            20



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CountChange denomination value ->
            let
                denominations =
                    model.denominations
                        |> List.map
                            (setDenominationCount denomination
                                (value
                                    |> String.toInt
                                    |> Maybe.withDefault 0
                                )
                            )
            in
            ( { model | denominations = denominations }, Cmd.none )

        SelectContent id ->
            ( model, selectInput id )

        ClearAll ->
            ( initialState, Cmd.none )


setDenominationCount : Denomination -> Int -> DenominationCount -> DenominationCount
setDenominationCount denomination count ( d, c ) =
    if denomination == d then
        ( denomination, count )

    else
        ( d, c )



-- MODEL


type Msg
    = CountChange Denomination String
    | SelectContent Int
    | ClearAll


type Denomination
    = FiveThousand
    | OneThousand
    | FiveHundred
    | OneHundred
    | Fifty
    | Twenty


type alias DenominationCount =
    ( Denomination, Int )


type alias Model =
    { denominations : List DenominationCount
    }



-- INIT


initialState : Model
initialState =
    Model
        [ ( FiveThousand, 0 )
        , ( OneThousand, 0 )
        , ( FiveHundred, 0 )
        , ( OneHundred, 0 )
        , ( Fifty, 0 )
        , ( Twenty, 0 )
        ]


init : () -> ( Model, Cmd Msg )
init _ =
    ( initialState, Cmd.none )



-- PORT


port selectInput : Int -> Cmd msg



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
