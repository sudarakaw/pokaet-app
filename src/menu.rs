use tauri::{App, Result};

#[cfg(desktop)]
pub fn setup(app: &App) -> Result<()> {
    use tauri::menu::{MenuBuilder, MenuItemBuilder, SubmenuBuilder};

    let menu_item_quit = MenuItemBuilder::new("Quit")
        .id("quit")
        .accelerator("CmdOrControl+Q")
        .build(app)?;

    let menu_app = SubmenuBuilder::new(app, "Pokaet")
        .items(&[&menu_item_quit])
        .build()?;

    let menubar = MenuBuilder::new(app).items(&[&menu_app]).build()?;

    app.set_menu(menubar)?;

    app.on_menu_event(|app, event| match event.id().as_ref() {
        "quit" => app.exit(0),
        _ => {}
    });

    Ok(())
}

#[cfg(not(desktop))]
pub fn setup(_: &App) -> Result<()> {
    Ok(())
}
