mod menu;

use tauri::{generate_handler, AppHandle};
use tauri_plugin_opener::OpenerExt;

#[tauri::command]
fn open_external_link(app: AppHandle, url: &str) {
    app.opener().open_url(url, None::<&str>).ok();
}

#[cfg_attr(mobile, tauri::mobile_entry_point)]
pub fn run() {
    tauri::Builder::default()
        .plugin(tauri_plugin_opener::init())
        .setup(|app| {
            menu::setup(app)?;

            Ok(())
        })
        .invoke_handler(generate_handler![open_external_link])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
